package main

import (
	"log"
	"flag"
	"http_nats_proxy/nats"
	"http_nats_proxy/listeners"
	oNats "github.com/nats-io/nats.go"
)

func main() {
	natsAddr  := flag.String("nats", oNats.DefaultURL, "NATS URL, in format of " + oNats.DefaultURL)
	proxy     := flag.String("proxy", "http://localhost:8080", "URL of the target proxy server, in format of http://target:port")
	flag.Parse()

	addresses := flag.Args()
	if len(addresses) == 0 {
		log.Fatal("Must define addresses to listen for")
	}

    nconn := nats.NewNATSConnection(*natsAddr)
    listeners.NewNATSListener(nconn.Conn, nconn.Enc, *proxy, addresses)
	select {}
}
