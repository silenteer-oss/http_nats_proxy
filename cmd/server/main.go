package main

import (
    "log"
    "strings"
    "net/http"
    "flag"
    "strconv"
    "io/ioutil"
    "time"
    "encoding/json"

    oNats "github.com/nats-io/nats.go"

    "http_nats_proxy/nats"
    "http_nats_proxy/types"
)

func main() {
    natsAddr  := flag.String("nats", oNats.DefaultURL, "NATS URL, in format of " + oNats.DefaultURL)
    port      := flag.Int("port", 8080, "Port")
	flag.Parse()

    nconn := nats.NewNATSConnection(*natsAddr)

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        log.Println("Message received")
        subject := request2Subject(r)
        log.Println("Sending message to " + subject)

        body, err := ioutil.ReadAll(r.Body)
        defer r.Body.Close()
        if err != nil {
            log.Fatal("Cannot read body")
        }

        if len(body) == 0 {
            body = nil
        }

        res := sendmsg(nconn, subject, &types.Message{
            Method: r.Method,
            Headers: r.Header,
            Body: body,
        })
        
        select {
        case <- time.After(3 * time.Second):
            log.Println("Operation timedout")
            w.Write([]byte("Timed out"))
            return
        case m := <- res:
            log.Println("Received response")
            b, _ := json.Marshal(m)
            w.Write(b)
            return
        }
    })
    
    log.Println("Accepting request on port: " + strconv.Itoa(*port))
    log.Fatal(http.ListenAndServe(":" + strconv.Itoa(*port), nil))
}

func sendmsg(n *nats.Connection, subject string, msg *types.Message) (chan types.Message) {
    res := make(chan types.Message)
    go func(msg *types.Message) {
        resb := &types.Message{}
        err  := n.Enc.Request(subject, msg, resb, 3 * time.Second)
        if err != nil {
            log.Println("Unable to receive response within 1 second")
        } else {
            res <- *resb
        }
    } (msg)

    return res
}

func request2Subject(ctx *http.Request) string {
    path   := ctx.URL.Path
    method := strings.ToUpper(ctx.Method)
    subject := method + "." + path
    return subject
}