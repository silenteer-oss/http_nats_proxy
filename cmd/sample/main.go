package main

import (
    "log"
    "net/http"
)

func main() {

    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		println(r.Header.Get("User-Agent"))
		w.Write([]byte("Hello"))
    })
    
    log.Fatal(http.ListenAndServe(":10000", nil))
}