package types

import (
	"net/http"
)

// Message is a simple struct
type Message struct {
    Method string `json:method`
    Headers http.Header `json:"headers"`
    Body []byte `json:"body"`
}