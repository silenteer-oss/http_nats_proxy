package listeners

import (
	"time"
	"net/http"
	"http_nats_proxy/nats"
)

// ClientProxy holder
type ClientProxy struct {
	client *http.Client
	nats   *nats.Connection
}

// NewClientProxy initialize Client
func NewClientProxy(nats *nats.Connection) *ClientProxy {
	netClient := &http.Client{
		Timeout: 10 * time.Second,
	}

	return &ClientProxy{
		client: netClient,
		nats  : nats,
	}
}