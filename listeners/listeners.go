package listeners

import (
	"log"
	"time"
	"net/http"
	"io/ioutil"
	"io"
	"bytes"

	"github.com/nats-io/nats.go"

	"http_nats_proxy/types"
)

// NewNATSListener returns a instructed listener
func NewNATSListener(conn *nats.Conn, enc *nats.EncodedConn, proxy string, addresses []string) {
	if len(addresses) == 0 {
		log.Fatal("No addresses to listen to")
	}
	
	client := &http.Client{
		Timeout: 5 * time.Second,
	}

	log.Println("Proxying request to: " + proxy)
	for _, s := range addresses {
		log.Println("Subscribing: " + s)
		enc.Subscribe(s, func(addr string, rep string, m *types.Message) {
			log.Println(addr + " " + rep)
			
			var body io.Reader
			if m.Body != nil {
				body = bytes.NewReader(m.Body)
			}

			req, err := http.NewRequest(m.Method, proxy, body)
			if err != nil {
				log.Println("Something wrong with creating the request", err)
			}
			req.Header.Add("Connection", "close")
	
			go func(enc *nats.EncodedConn, client *http.Client, req *http.Request) {
				res, err := client.Do(req)
				if err != nil {
					log.Println("Received an error ")
				}
	
				body, err := ioutil.ReadAll(res.Body)
				defer res.Body.Close()
				if err != nil {
					log.Println("Unable to read body")
				}
				
				if len(body) == 0 {
					body = nil
				}
	
				data := &types.Message {
					Headers: res.Header,
					Body: body,
				}
				
				enc.Request(rep, data, nil, 2 * time.Second)
			} (enc, client, req)
			
		})
	}
	
}
