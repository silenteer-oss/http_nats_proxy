package main

import (
	"log"
	"flag"
	"github.com/nats-io/nats.go"
)

func main() {
	port := flag.Int("port", 8080, "Server port")
	nats := flag.String("nats", nats.DefaultURL, "NATS URL")

	log.Println(*port)
	log.Println(*nats)
}