A very simple http to nats and vice versa

There are 2 components in this product

## A server

Will listen at a desired port, capture all request and forwarding them all to nats server.

Start the process using `go run ./cmd/server -port 8080 -nats nats://localhost:4222`

## A client

Will subscribe to NATS at choosen addresses and forward them to a http server

Start the process using `go run ./cmd/client -proxy http://localhost:9000 -nats://localhost:4222 "GET.>" "POST.>"`

## Subject scheme

Subject is designed following NATS subject scheme

GET: /api/chat/... will be converted to GET.api.chat.create

A client can listen to that subject by either

- GET.>, all GET request to NATS
- GET.api.>, all GET request to api path
- GET.api.chat.>, all GET request to chat service

## NATS message format

For now, the format is rather simple, it's a JSON message containing

- method: string, one of HTTP Methods in upper case
- headers: map<string, list<string>>
- body: base64 of byte[]